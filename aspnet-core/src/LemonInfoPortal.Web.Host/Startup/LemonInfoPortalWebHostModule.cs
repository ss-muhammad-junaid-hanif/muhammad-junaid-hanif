﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using LemonInfoPortal.Configuration;

namespace LemonInfoPortal.Web.Host.Startup
{
    [DependsOn(
       typeof(LemonInfoPortalWebCoreModule))]
    public class LemonInfoPortalWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public LemonInfoPortalWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(LemonInfoPortalWebHostModule).GetAssembly());
        }
    }
}
