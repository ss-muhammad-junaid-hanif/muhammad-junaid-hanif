using Microsoft.AspNetCore.Antiforgery;
using LemonInfoPortal.Controllers;

namespace LemonInfoPortal.Web.Host.Controllers
{
    public class AntiForgeryController : LemonInfoPortalControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
