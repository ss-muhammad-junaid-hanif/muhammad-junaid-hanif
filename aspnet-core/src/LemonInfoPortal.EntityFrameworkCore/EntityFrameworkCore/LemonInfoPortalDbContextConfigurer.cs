using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace LemonInfoPortal.EntityFrameworkCore
{
    public static class LemonInfoPortalDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<LemonInfoPortalDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<LemonInfoPortalDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
