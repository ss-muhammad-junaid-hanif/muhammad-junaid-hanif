﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using LemonInfoPortal.Authorization.Roles;
using LemonInfoPortal.Authorization.Users;
using LemonInfoPortal.MultiTenancy;

namespace LemonInfoPortal.EntityFrameworkCore
{
    public class LemonInfoPortalDbContext : AbpZeroDbContext<Tenant, Role, User, LemonInfoPortalDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public LemonInfoPortalDbContext(DbContextOptions<LemonInfoPortalDbContext> options)
            : base(options)
        {
        }
    }
}
