﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using LemonInfoPortal.Configuration;
using LemonInfoPortal.Web;

namespace LemonInfoPortal.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class LemonInfoPortalDbContextFactory : IDesignTimeDbContextFactory<LemonInfoPortalDbContext>
    {
        public LemonInfoPortalDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LemonInfoPortalDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            LemonInfoPortalDbContextConfigurer.Configure(builder, configuration.GetConnectionString(LemonInfoPortalConsts.ConnectionStringName));

            return new LemonInfoPortalDbContext(builder.Options);
        }
    }
}
