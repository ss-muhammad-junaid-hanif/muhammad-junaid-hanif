﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using LemonInfoPortal.MultiTenancy.Dto;

namespace LemonInfoPortal.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

