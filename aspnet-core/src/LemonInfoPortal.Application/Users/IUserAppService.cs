using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using LemonInfoPortal.Roles.Dto;
using LemonInfoPortal.Users.Dto;

namespace LemonInfoPortal.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
