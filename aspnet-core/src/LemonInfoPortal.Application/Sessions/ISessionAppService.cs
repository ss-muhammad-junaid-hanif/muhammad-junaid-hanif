﻿using System.Threading.Tasks;
using Abp.Application.Services;
using LemonInfoPortal.Sessions.Dto;

namespace LemonInfoPortal.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
