﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using LemonInfoPortal.Authorization;

namespace LemonInfoPortal
{
    [DependsOn(
        typeof(LemonInfoPortalCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class LemonInfoPortalApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<LemonInfoPortalAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(LemonInfoPortalApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
