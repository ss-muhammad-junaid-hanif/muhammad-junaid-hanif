﻿using System.Threading.Tasks;
using Abp.Application.Services;
using LemonInfoPortal.Authorization.Accounts.Dto;

namespace LemonInfoPortal.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
