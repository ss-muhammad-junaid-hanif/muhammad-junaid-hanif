﻿using System.Threading.Tasks;
using LemonInfoPortal.Configuration.Dto;

namespace LemonInfoPortal.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
