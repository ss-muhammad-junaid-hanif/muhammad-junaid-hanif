﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using LemonInfoPortal.Configuration.Dto;

namespace LemonInfoPortal.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : LemonInfoPortalAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
