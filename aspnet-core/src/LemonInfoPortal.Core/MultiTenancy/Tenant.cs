﻿using Abp.MultiTenancy;
using LemonInfoPortal.Authorization.Users;

namespace LemonInfoPortal.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
