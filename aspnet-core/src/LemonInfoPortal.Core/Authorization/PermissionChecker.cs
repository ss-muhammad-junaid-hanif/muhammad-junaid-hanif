﻿using Abp.Authorization;
using LemonInfoPortal.Authorization.Roles;
using LemonInfoPortal.Authorization.Users;

namespace LemonInfoPortal.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
