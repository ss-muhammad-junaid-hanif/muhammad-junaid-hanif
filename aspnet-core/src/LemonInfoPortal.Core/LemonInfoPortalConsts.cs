﻿namespace LemonInfoPortal
{
    public class LemonInfoPortalConsts
    {
        public const string LocalizationSourceName = "LemonInfoPortal";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
