﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace LemonInfoPortal.Localization
{
    public static class LemonInfoPortalLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(LemonInfoPortalConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(LemonInfoPortalLocalizationConfigurer).GetAssembly(),
                        "LemonInfoPortal.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
