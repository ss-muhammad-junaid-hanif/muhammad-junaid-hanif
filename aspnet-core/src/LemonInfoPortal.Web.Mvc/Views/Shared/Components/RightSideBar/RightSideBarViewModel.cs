﻿using LemonInfoPortal.Configuration.Ui;

namespace LemonInfoPortal.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
