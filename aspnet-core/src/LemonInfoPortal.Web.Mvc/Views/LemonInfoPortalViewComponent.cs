﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace LemonInfoPortal.Web.Views
{
    public abstract class LemonInfoPortalViewComponent : AbpViewComponent
    {
        protected LemonInfoPortalViewComponent()
        {
            LocalizationSourceName = LemonInfoPortalConsts.LocalizationSourceName;
        }
    }
}
