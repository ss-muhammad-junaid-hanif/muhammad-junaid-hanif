﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace LemonInfoPortal.Web.Views
{
    public abstract class LemonInfoPortalRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected LemonInfoPortalRazorPage()
        {
            LocalizationSourceName = LemonInfoPortalConsts.LocalizationSourceName;
        }
    }
}
