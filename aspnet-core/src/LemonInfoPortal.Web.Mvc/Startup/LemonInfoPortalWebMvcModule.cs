﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using LemonInfoPortal.Configuration;

namespace LemonInfoPortal.Web.Startup
{
    [DependsOn(typeof(LemonInfoPortalWebCoreModule))]
    public class LemonInfoPortalWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public LemonInfoPortalWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<LemonInfoPortalNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(LemonInfoPortalWebMvcModule).GetAssembly());
        }
    }
}
