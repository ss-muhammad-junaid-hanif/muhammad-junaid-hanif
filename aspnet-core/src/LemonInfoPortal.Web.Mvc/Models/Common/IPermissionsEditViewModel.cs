﻿using System.Collections.Generic;
using LemonInfoPortal.Roles.Dto;

namespace LemonInfoPortal.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}