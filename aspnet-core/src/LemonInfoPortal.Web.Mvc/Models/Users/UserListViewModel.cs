using System.Collections.Generic;
using LemonInfoPortal.Roles.Dto;
using LemonInfoPortal.Users.Dto;

namespace LemonInfoPortal.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
