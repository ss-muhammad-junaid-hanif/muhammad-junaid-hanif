﻿using System.Collections.Generic;
using LemonInfoPortal.Roles.Dto;

namespace LemonInfoPortal.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
