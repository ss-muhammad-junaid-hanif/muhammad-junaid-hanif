﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using LemonInfoPortal.Controllers;

namespace LemonInfoPortal.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : LemonInfoPortalControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
