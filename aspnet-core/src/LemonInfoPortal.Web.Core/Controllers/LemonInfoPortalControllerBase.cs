using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace LemonInfoPortal.Controllers
{
    public abstract class LemonInfoPortalControllerBase: AbpController
    {
        protected LemonInfoPortalControllerBase()
        {
            LocalizationSourceName = LemonInfoPortalConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
